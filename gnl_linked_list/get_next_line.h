/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmechich <hmechich@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/14 12:45:41 by hmechich          #+#    #+#             */
/*   Updated: 2021/12/14 16:28:41 by hmechich         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <stdlib.h>
# include <unistd.h>

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 42
# endif

typedef struct	s_node
{
	char			*content;
	struct s_node	*next;
}	t_node;

char	*get_next_line(int fd);
t_node	*create_lst(char *content);
void	insert_after_node(t_node *node_to_insert_after, t_node *node_to_insert);
char	*get_last_node(t_node *head);
char	*ft_strdup(const char *s);
#endif
