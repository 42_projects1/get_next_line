/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hmechich <hmechich@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/14 12:44:34 by hmechich          #+#    #+#             */
/*   Updated: 2021/12/14 16:32:08 by hmechich         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h> //------------------------------------------------------------

char	*get_next_line(int fd)
{
	t_node	*lst;
	char	*buffer;
	int		read_value;
	
	
	if (fd < 0 || BUFFER_SIZE <= 0)
		return (NULL);
	buffer = malloc(sizeof(char) * BUFFER_SIZE + 1);	
	if (!buffer)
		return (NULL);
	read_value = 1;
	lst = NULL;
	while (read_value != 0)
	{
		read_value = read(fd, buffer, BUFFER_SIZE);
		if (read_value == -1)
			return (free(buffer), NULL);
		buffer[read_value] = '\0';
		printf("Arthur\n");
		if (!lst)
			lst = create_lst(ft_strdup(buffer));
		else
			insert_after_node(lst, create_lst(buffer));
		printf("lst->content: %s\n", lst->content);
	}
	free(buffer);
	return (get_last_node(lst));
}

t_node	*create_lst(char *content)
{
	t_node	*result;

	result = malloc(sizeof(t_node));
	result->content = content;
	printf("result->content: %s\n", result->content);
	result->next = NULL;
	return (result);
}

void	insert_after_node(t_node *node_to_insert_after, t_node *node_to_insert)
{
	node_to_insert->next = node_to_insert_after->next;
	node_to_insert_after->next = node_to_insert;
}

char	*get_last_node(t_node *head)
{
	t_node	*tmp;
	char	*content;

	tmp = head;
	printf("testchelou %s\n", tmp->next->content);
	while (tmp != NULL)
	{
		content = tmp->content;
		printf("content: %s\n", content);
		tmp = tmp->next;
	}
	return (content);
}

char	*ft_strdup(const char *s)
{
	char	*new;
	int		i;

	i = 0;
	while (s[i])
		i++;
	new = malloc(sizeof (char) * i + 1);
	if (new == NULL)
		return (NULL);
	i = 0;
	while (s[i] || s[i] != '\n')
	{
		new[i] = s[i];
		i++;
	}
	new[i] = 0;
	return (new);
}
